<?php
error_reporting(E_ALL);
ini_set('display_errors', 'On');

require_once __DIR__ . '/vendor/autoload.php';
require __DIR__ . '/database/db.php';
/**
* 
*/

use FacebookAds\Object\Fields\AdUserFields;
use Facebook\Facebook;
use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;
use FacebookAds\Api;
use FacebookAds\Object\AdUser;
use FacebookAds\Object\Campaign;
use FacebookAds\Object\Fields\CampaignFields;

use FacebookAds\Object\Values\InsightsPresets;
use FacebookAds\Object\Fields\InsightsFields;
use FacebookAds\Object\AdSet;
use FacebookAds\Object\Fields\AdCreativeFields;
use FacebookAds\Object\AdAccount;
use FacebookAds\Object\Values\ArchivableCrudObjectEffectiveStatuses;

use FacebookAds\Object\Fields\AdSetFields;
use Facebook\FacebookRequest;
use FacebookAds\Object\Values\InsightsActionBreakdowns;

use FacebookAds\Object\Fields\AdFields;
use FacebookAds\Object\Ad;
// Init PHP Sessions
session_start();


$fb = new Facebook([
  'app_id' => '297565087299669',
  'app_secret' => 'd80d7d67abe7902bfbac0d3077381802',
]);

$helper = $fb->getRedirectLoginHelper();

if (!isset($_SESSION['facebook_access_token'])) {
  $_SESSION['facebook_access_token'] = null;
}

if (!$_SESSION['facebook_access_token']) {
  $helper = $fb->getRedirectLoginHelper();
  try {
    $_SESSION['facebook_access_token'] = (string) $helper->getAccessToken();
  } catch(FacebookResponseException $e) {
    // When Graph returns an error
    echo 'Graph returned an error: ' . $e->getMessage();
    exit;
  } catch(FacebookSDKException $e) {
    // When validation fails or other local issues
    echo 'Facebook SDK returned an error: ' . $e->getMessage();
    exit;
  }
}

if ($_SESSION['facebook_access_token']) {
  echo "You are logged in!";

  echo "<br/>";

  Api::init(
  '297565087299669', // App ID
  'd80d7d67abe7902bfbac0d3077381802',
  $_SESSION['facebook_access_token']); // Your user access token


$account = new AdAccount  ('act_314892195353164');
$campaigns = $account->getCampaigns(array(
  CampaignFields::NAME,
  CampaignFields::OBJECTIVE,
  CampaignFields::ID
), array(
    CampaignFields::EFFECTIVE_STATUS => array(
    ArchivableCrudObjectEffectiveStatuses::ACTIVE,
  ),
));


foreach ($campaigns as $campaign ){
  // var_dump($campaign);
  // die();
  $campaign_id =  $campaign->id;
  $campaign_name =  $campaign->name;
$campaign_name1 =  str_replace(' ', '-', $campaign_name);
$campaign_name2 = preg_replace('/[^A-Za-z0-9\-]/', '', $campaign_name1);

$campaign = new Campaign($campaign_id);
$params = array(
  AdFields::EFFECTIVE_STATUS => array(
    Ad::STATUS_ACTIVE,
  ),
);
$ads = $campaign->getAds(array(
  AdFields::ID,
),$params);






// foreach($insights as $i){

//   var_dump($i);

// }


foreach ($ads as $ad) {
  $ad_id =  $ad->{AdFields::ID}.PHP_EOL;
 
$d = new AdSet(ad_id);

  //  $insights = $ads->getInsights(array(
//   InsightsFields::IMPRESSIONS,
//   InsightsFields::INLINE_LINK_CLICKS,
//   InsightsFields::SPEND,
//   InsightsFields::UNIQUE_CLICKS,
//   InsightsFields::CPM,
//   InsightsFields::CPP,
//   InsightsFields::DATE_START,
//   InsightsFields::DATE_STOP,
//   InsightsFields::ACTIONS,
//   InsightsFields::TOTAL_ACTION_VALUE,
//   InsightsFields::SOCIAL_IMPRESSIONS,
//   InsightsFields::REACH,
//   InsightsFields::WEBSITE_CTR,
//   InsightsFields::COST_PER_ACTION_TYPE,
//   InsightsFields::TOTAL_ACTIONS,
//   InsightsFields::ACTION_VALUES
// ), $params);
}


// die();
//   $c = new Campaign($campaign->id);
//   $params = array(
//     'date_preset' => InsightsPresets::TODAY,
//   'action_breakdowns' => array(
//     InsightsActionBreakdowns::ACTION_DESTINATION,
//     InsightsActionBreakdowns::ACTION_TYPE,
//   ),
// );

//   $insights = $campaign->getInsights(array(
//   InsightsFields::IMPRESSIONS,
//   InsightsFields::INLINE_LINK_CLICKS,
//   InsightsFields::SPEND,
//   InsightsFields::UNIQUE_CLICKS,
//   InsightsFields::CPM,
//   InsightsFields::CPP,
//   InsightsFields::DATE_START,
//   InsightsFields::DATE_STOP,
//   InsightsFields::ACTIONS,
//   InsightsFields::TOTAL_ACTION_VALUE,
//   InsightsFields::SOCIAL_IMPRESSIONS,
//   InsightsFields::REACH,
//   InsightsFields::WEBSITE_CTR,
//   InsightsFields::COST_PER_ACTION_TYPE,
//   InsightsFields::TOTAL_ACTIONS,
//   InsightsFields::ACTION_VALUES
// ), $params);

// foreach($insights as $i){

//   var_dump($i);
//   die();
//  $impressions =  $i->impressions;
//   $spend =  (float)$i->spend;

//   $unique_clicks = $i->unique_clicks;
//   $inline_link_clicks  = $i->inline_link_clicks;
//   $cpm =  $i->cpm;
//   $cpm1  = round($cpm,2);
//   $cpp = $i->cpp;
//   $cpp1 = round($cpp,2);

//   $date_start = $i->date_start;
//   $date_stop  = $i->date_stop;
//   $total_action_value = $i->total_action_value;
//   $social_impressions =$i->social_impressions;
//   $reach = $i->reach; 
//   if($spend > 0 && $unique_clicks > 0){
//   $cpc =  (float)($spend/$unique_clicks);

//   $cpc1 = round($cpc,2);
// }
// else{
//   $cpc1 = (float)(0);
// }


//   $website_ctr = (int)($i->website_ctr);
// $total_action  = $i->total_actions;

// // prepare and bind
// // $stmt1 = "INSERT INTO facebooklead (campaign_id, campaign_name  , spend,unique_clicks,inline_link_clicks,cpm,cpp,date_start, date_stop, total_action_value,social_impressions,reach,cpc, website_ctr, total_actions, created_at,updated_at) VALUES 
// // ($campaign_id, '{$campaign_name2}', $spend, $unique_clicks, $inline_link_clicks, $cpm1, $cpp1, '{$date_start}', '{$date_stop}', $total_action_value,$social_impressions, $reach, $cpc1,$website_ctr,$total_action,NOW(),NOW())";


// // if ($con->query($stmt1) === TRUE) {
// //     echo "New record created successfully";
// // } else {
// //     echo "Error: " . $stmt1 . "<br>" . $con->error;
// // }

//  $f  = $i->actions;

// foreach((array)$f as $act){
// if($act["action_type"] == "offsite_conversion.fb_pixel_purchase"){
//     $total_purchases = (float)$act["value"];

//    $stmt4 = "UPDATE facebooklead SET total_purchases = $total_purchases WHERE campaign_id = $campaign_id";

//        if ($con->query($stmt4) === TRUE) {
//     echo "New record created successfully";
// } else {
//     echo "Error: " . $stmt4 . "<br>" . $con->error;
// }

//    }
// }

//   $r  = $i->cost_per_action_type;

//   foreach((array)$r as $val){
//     if ($val["action_type"] == 'offsite_conversion.fb_pixel_purchase') {
//         $purchase_amount  = (float)$val["value"];   
    
//       $stmt3 ="UPDATE facebooklead SET purchase_amount = $purchase_amount WHERE campaign_id = $campaign_id";

//     if ($con->query($stmt3) === TRUE) {
//     echo "New record created successfully";
// } else {
//     echo "Error: " . $stmt3 . "<br>" . $con->error;
// }
  
//   }
//   }  
//   }
}
echo "New records created successfully";


} else {
  $permissions = ['ads_management','ads_read', 'public_profile'];
  $loginUrl = $helper->getLoginUrl('http://localhost/facebook-api/', $permissions);
  echo '<a href="' . $loginUrl . '">Log in with Facebook</a>';
} 